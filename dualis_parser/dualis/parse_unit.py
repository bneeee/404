from bs4 import BeautifulSoup
from os.path import dirname, join

current_dir = dirname(__file__)
file_path = join(current_dir, "./test3.html")
with open(file_path, 'r') as f:
    contents =f.read()
    soup = BeautifulSoup(contents, 'html.parser')
    h1 = soup.find("h1").text.strip()
    unit = {'name': h1.replace("\n", " ").replace("\r", ""), 'exams': []}

    tables = soup.find_all("table",{"class":"tb"})
    
    rows = tables[0].find_all("tr")
    i = 0
    for row in rows:
        data = row.find_all("td",{"class":"tbdata"})
        if len(data)!=0:
            countAttempts = 1
            currentName = data[1].text.strip()
            currentDiscription = rows[i-1].find_all("td")[0].text.strip()
            currentDate = data[0].text.strip()
            currentGrade = data[3].text.strip()
            for item in unit['exams']:
                if (currentName == item['name']) and (currentDiscription == item['description']) and (currentDate == item['date']):
                    countAttempts = countAttempts +1
            exam = {'name': currentName,"description":currentDiscription, 'date': currentDate, 'grade': currentGrade, 'attempt': countAttempts}
            unit['exams'].append(exam)
        i=i+1
    print(unit)
        