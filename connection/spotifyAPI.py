import spotipy
import spotipy.util as util
import spotipy.oauth2 as oauth2
from array import array

scope = "streaming user-modify-playback-state user-read-playback-state"
client_id = "a78678509ceb4f3583b49d08a4e753f7"
client_secret = "86504f68683a46e48ea87800b83b2f3b"
#redirect_uri = "http://localhost:5000/api/spotifyOAuth"
redirect_uri = "https://styx.cf/api/spotifyOAuth"
soauth = oauth2.SpotifyOAuth(
    client_id, client_secret, redirect_uri, scope=scope)


def getAuthorizeURL():
    return soauth.get_authorize_url()


def getAccessToken(authorizeURL):
    cachedToken = soauth.get_cached_token()
    if(cachedToken and not soauth.is_token_expired(cachedToken['access_token'])):
        return cachedToken['access_token']
    else:
        code = soauth.parse_response_code(authorizeURL)
        token = soauth.get_access_token(code)['access_token']
        return token


def getDevices(token):
    sp = spotipy.Spotify(auth=token)
    return sp.devices()


def playSong(token, songName):
    sp = spotipy.Spotify(auth=token)
    song = sp.search(q=songName, type="track", limit=1)
    if(not song):
        return
    device_id = "bac09ab917c94653b8f9d33b624b92a0d40a675b"
    song_id = song['tracks']['items'][0]['id']
    song_uris = ["spotify:track:" + song_id]
    sp.start_playback(device_id, uris=song_uris)
