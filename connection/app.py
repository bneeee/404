from flask import Flask, request, redirect
import zmq
import time
import sys
import spotifyAPI
import dbstuff
import json

context = zmq.Context()
socket = context.socket(zmq.PUB)

app = Flask(__name__)


@app.route("/")
def hello():
    return "<h1 style='color:blue'>Hello There!</h1>"


def publish_message(message):
    try:
        socket.bind("tcp://0.0.0.0:4444")
        time.sleep(1)
        socket.send_string(message)
    except Exception as e:
        print("error {}".format(e))
    finally:
        socket.unbind("tcp://0.0.0.0:4444")


@app.route("/api/speechText/", methods=['POST'])
def speechText():
    topic = "Speech"
    data = request.json
    print(data)
    messagedata = data['text']
    publish_message(messagedata)
    return messagedata


@app.route("/api/spotify", methods=['GET'])
def spotify():
    topic = "Spotify"
    authUrl = spotifyAPI.getAuthorizeURL()
    return redirect(authUrl)


@app.route("/api/spotifyOAuth/", methods=['GET'])
def spotifyOAuth():
    topic = "SpotifyOAuth"
    token = spotifyAPI.getAccessToken(request.url)
    if(token):
        spotifyAPI.playSong(token, "Some Say")
        return spotifyAPI.getDevices(token)
    return "Auth failed"

@app.route("/api/saveSpotifyToken",methods=['POST'])
def saveSpotifyToken():
    data = request.json
    dbstuff.saveSpotifyToken(data['token'],int(data['userid']))
    return ""

@app.route("/api/getSpotifyToken",methods=['POST'])
def getSpotifyToken():
    data = request.json
    return json.dumps(dbstuff.getSpotifyToken(data['userid'])) 

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
