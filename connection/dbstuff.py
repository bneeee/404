import psycopg2

class DBConnection:
    instance = None
    con = None

    def __new__(cls):
        if DBConnection.instance is None:
            DBConnection.instance = object.__new__(cls)
        return DBConnection.instance

    def __init__(self):
        if DBConnection.con is None:
            try:
                DBConnection.con = psycopg2.connect(database="digital_assistent",user="postgres",password="docker",port="5432",host="postgres-db")
                print('Database connection opened.')
            except psycopg2.DatabaseError as db_error:
                print("Erreur :\n{0}".format(db_error))
    def __del__(self):
        if DBConnection.con is not None:
            DBConnection.instance = None
            DBConnection.con.close()
            print('Database connection closed.')

def saveSpotifyToken(token: str, userid: str):
    conn = DBConnection()
    cursor = conn.con.cursor()
    query = """INSERT INTO user_preferences (key,value,user_id)
    VALUES (%s,%s,%s)
    ON CONFLICT (key,user_id)
    DO UPDATE
    SET value = excluded.value;"""
    key = "spotifyToken"
    cursor.execute(query,(key,token,userid))
    conn.con.commit()

def getSpotifyToken(userid: str):
    conn = DBConnection()
    cursor = conn.con.cursor()
    query = """SELECT * FROM user_preferences
    WHERE user_id = %s
    AND key='spotifyToken';"""
    cursor.execute(query,(userid))
    result = cursor.fetchall()
    print(result)
    return result