import validators
import urllib.request
import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import re
import psycopg2
import sys

class DBConnection:
    instance = None
    con = None

    def __new__(cls):
        if DBConnection.instance is None:
            DBConnection.instance = object.__new__(cls)
        return DBConnection.instance

    def __init__(self):
        if DBConnection.con is None:
            try:
                DBConnection.con = psycopg2.connect(database="digital_assistent",user="postgres",password="docker",port="5432",host="localhost")
                print('Database connection opened.')
            except psycopg2.DatabaseError as db_error:
                print("Erreur :\n{0}".format(db_error))
    def __del__(self):
        if DBConnection.con is not None:
            DBConnection.instance = None
            DBConnection.con.close()
            print('Database connection closed.')

def checkURL(url :str):
    #Check if url is valid
    result = validators.url(url)
    if result != True:
        return False

    #check if url is reachable
    result = urllib.request.urlopen(url).getcode()
    if result !=200:
        return False
    return True

def getPageContent(url :str):
    #get content from url
    response = requests.get(url=url)
    return response.content

def parseStuff(pageContent :str):
    #get only tbody data from page
    leftIndex = pageContent.index('<tbody')
    rightIndex = pageContent.rindex("</tbody")+8
    pageContent = pageContent[leftIndex:rightIndex]
    #further improvements for better parsing
    pageContent = pageContent.replace("&nbsp;", "&#160;").replace("<br>", "<br/>").replace("<a href=\".*[<>].*\">.*</a>", "")
    #Create Soup Object for HTML Parsing
    soup = BeautifulSoup(pageContent,features="html.parser")
    #Get all rows
    rows = soup.findChildren('tr')
    for row in rows:
        importRow(row)

def getTime(block :BeautifulSoup,baseDate :datetime):
    #get time from html table
    timeRaw = block.findChildren("span",class_="tooltip")[0].findChildren("div")[1].getText()
    #get time in format HH:MM from raw string
    beginEndTime = re.search("[0-9][0-9]:[0-9][0-9]-[0-9][0-9]:[0-9][0-9]",timeRaw).group(0)
    #get begin time
    beginTime = beginEndTime.split("-")[0]
    #get end time
    endTime = beginEndTime.split("-")[1]
    timeDiff = 0
    if timeRaw[0:2] == 'Mo':
        timeDiff = 0
    elif timeRaw[0:2] == 'Di':
        timeDiff = 1
    elif timeRaw[0:2] == 'Mi':
        timeDiff = 2
    elif timeRaw[0:2] == 'Do':
        timeDiff = 3
    elif timeRaw[0:2] == 'Fr':
        timeDiff = 4
    elif timeRaw[0:2] == 'Sa':
        timeDiff = 5
    elif timeRaw[0:2] == 'So':
        timeDiff = 6
    #add day difference to begindate and replace with correct times
    beginDate = baseDate.replace(hour=int(beginTime.split(":")[0]),minute=int(beginTime.split(":")[1])) + timedelta(days=timeDiff)
    #add day difference to enddate and replace with correct times
    endDate = baseDate.replace(hour=int(endTime.split(":")[0]),minute=int(endTime.split(":")[1])) + timedelta(days=timeDiff)
    
    return {
        "begin":beginDate,
        "end":endDate
    }

def getDescription(cells):
    description = {
        "Ressourcen": [],
        "Personen": [],
        "Veranstaltungsname": []
    }

    #for loop with index
    for idx,cell in enumerate(cells):
        #check for elements in table
        if cell.getText() == "Ressourcen:" or cell.getText() == "Bemerkungen:":
            description['Ressourcen'].append(cells[idx+1].getText())
        if cell.getText() == "Personen:":
            description['Personen'].append(cells[idx+1].getText())
        if cell.getText() == "Veranstaltungsname:" or cell.getText() == "Titel:":
            description['Veranstaltungsname'].append(cells[idx+1].getText())

    return description

    
def importLecture(block :BeautifulSoup):
    #get all children from cell
    allChildren = block.findChildren()

    #get all descriptive elements (name,people,ressources)
    lecture = getDescription(allChildren)
    #should be current day
    day = '18/11/2019 00:00'
    dt = datetime.strptime(day, '%d/%m/%Y %H:%M')
    #get time element for a lecture and append it to object
    lecture.update(getTime(block,dt))
    insertLectureToDB(lecture)
    print(lecture)

def importRow(row :BeautifulSoup):
    #get all table cells with class "week_block"
    cells = row.find_all("td",class_="week_block")
    if len(cells) == 0:
        return
    #parse lecture if there are cells
    importLecture(cells[0])

def getUrlByDate(baseurl: str,year: int, month: int, day: int):
    #split parameters from host
    baseurl = baseurl.split("?")
    key = None
    if len(baseurl) == 0:
        return -1
    baseurl = baseurl[1]
    #split all parameters from query
    baseurl = baseurl.split("&")
    if len(baseurl) ==0:
        return -1
    for baseurlPart in baseurl:
        #extract key
        if baseurlPart.startswith("key="):
            key = baseurlPart[4::]
    if key == None:
        return -1
    return "https://rapla.dhbw-stuttgart.de/rapla?key=" + key + "&day=" + str(day) + "&month=" + str(month) + "&year=" + str(year)

def insertLectureToDB(lecture):
    #create connection with singelton pattern
    conn = DBConnection()
    #create cursor from connection
    cursor = conn.con.cursor()
    query = """INSERT INTO calendar (title,description,begintime,endtime,place,type,user_id)
    VALUES (%s,%s,%s,%s,%s,%s,%s)"""

    #concat ressourcen and personen from lecture
    description = lecture['Ressourcen'] + lecture['Personen']
    #Join elements from veranstaltungsname with , as separator
    veranstaltungsname = ", ".join(lecture['Veranstaltungsname'])
    cursor.execute(query,(veranstaltungsname,description,lecture['begin'],lecture['end'],"Rotebühlplatz 41B, Stuttgart","lecture",1))
    #commit new entry in db
    conn.con.commit()

def parseWeeks(baseurl: str,weeks: int):
    if not checkURL(baseurl):
        exit -1
    thisWeek = datetime.now()
    thisWeek = thisWeek - timedelta(days=thisWeek.weekday())
    for idx in range(weeks):
        currentUrl = getUrlByDate(baseurl,thisWeek.year,thisWeek.month,thisWeek.day)
        pageContent = getPageContent(currentUrl).decode('utf-8')
        parseStuff(pageContent)
        #add one week to date
        thisWeek = thisWeek + timedelta(days=7)

def saveSpotifyToken(token :str, userid :str):
    conn = DBConnection()
    cursor = conn.con.cursor()
    query = """INSERT INTO user_preferences (key,value,user_id)
    VALUES (%s,%s,%s)
    ON CONFLICT (key,user_id)
    DO UPDATE
    SET value = excluded.value;"""
    key = "spotifyToken"
    cursor.execute(query,(key,token,userid))
    conn.con.commit()

if __name__ == "__main__":
    if len(sys.argv) ==3:
        #https://rapla.dhbw-stuttgart.de/rapla?key=txB1FOi5xd1wUJBWuX8lJhGDUgtMSFmnKLgAG_NVMhDIIewgVA_jMwZc3V6HJenC
        parseWeeks(sys.argv[1],int(sys.argv[2]))
    saveSpotifyToken("lschwwuuullzub",1)
