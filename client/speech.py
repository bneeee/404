import pyttsx3
import _thread
import threading
import speech_recognition as sr
import pythoncom
import os


class outputSpeech(threading.Thread):
    def __init__(self,text="Ich bin nicht betronken aber Bier trinken ist wichtig."):
        self.text = text
        threading.Thread.__init__(self)
    def run(self):
        pythoncom.CoInitialize()
        try:
            ## Initialisierung
            engine = pyttsx3.init()
            voice_id = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_DE-DE_HEDDA_11.0"
            ## Geschwindigkeit
            rate = engine.getProperty('rate')   # getting details of current speaking rate
            engine.setProperty('rate', 100)     # setting up new voice rat<e

            ## Auswahl des Gender
            voices = engine.getProperty('voices')
            #engine.setProperty('voice', voices[1].id) 
            ## Ausgabe
            engine.setProperty('voice', voice_id)

            engine.say(self.text)
            engine.runAndWait()
        except Exception as e:
            print('Exception: ', e)

class listenMicrophoneForKeywords(threading.Thread):
    def __init__(self,x): 
        self.x = x
        threading.Thread.__init__(self) 

    def run(self): 
        # link: https://github.com/Uberi/speech_recognition/blob/master/examples/microphone_recognition.py
        # obtain audio from the microphone
        keyword = "stop"
        userSpeech = ""
        r = sr.Recognizer() 
        with sr.Microphone() as source:
            print("Say something!")
            audio = r.listen(source)
        # recognize speech using Google Speech Recognition
        try:
            # for testing purposes, we're just using the default API key
            # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
            # instead of `r.recognize_google(audio)`
            userSpeech = r.recognize_google(audio)
            print("Google Speech Recognition thinks you said " + userSpeech)
            
            if keyword in userSpeech:
                #self.x.stop()
                os._exit(1)
        except sr.UnknownValueError:
            print("Google Speech Recognition could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))
        except Exception as e:
            print("Exception: ", e)

#x = outputSpeech()
#x.start()
#y = listenMicrophoneForKeywords()
#y.start()