import zmq
import speech
import threading

def sayMessage(message):
    x = speech.outputSpeech(message)
    x.start()
    y = speech.listenMicrophoneForKeywords(x)
    y.start()

port = "4444"

context = zmq.Context()
socket = context.socket(zmq.SUB)

socket.connect("tcp://188.68.38.42:%s" % port)
socket.setsockopt_string(zmq.SUBSCRIBE, '')

while True:
    #  Wait for next request from client
    message = socket.recv_string()
    print("Received request: %s" % message)
    sayMessage(message)