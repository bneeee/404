var collapseAll = false;
var eveningSoundType = "sea";

$(document).ready(function () {
  init_sections();
  init_eveningSection();
  init_raplaSection();
  init_testSection();
});

function init_sections() { 
// Update section heights
  $.each($("section"), function (indexInArray, valueOfElement) {
    adjustToContentHeight($(this));
  });

  // Init section titles
  $("section h2").click(function (e) {
    e.preventDefault();
    $(this).parent().toggleClass("fold");

    if ($("section.fold").length == 0) {
      $("#btn-collapse-all").html("Alle einklappen");
      collapseAll = true;
    }
    if ($("section:not(.fold)").length == 0) {
      $("#btn-collapse-all").html("Alle aufklappen");
      collapseAll = false;
    }
  });

  // Init collapse all button
  $("#btn-collapse-all").click(function (e) {
    e.preventDefault();
    if (collapseAll) {
      $("section").addClass("fold");
      $(this).html("Alle aufklappen");
    }
    else {
      $("section").removeClass("fold");
      $(this).html("Alle einklappen");
    }
    collapseAll = !collapseAll;
  });
}

function init_eveningSection() { 
  $('#evening-sound-type').change(function(e){
    eveningSoundType = $(this).val();
    switch(eveningSoundType){
      case "song":
        $('#input-spotify-song-evening, label[for="input-spotify-song-evening"]').show();
        break;
      default:
        $('#input-spotify-song-evening, label[for="input-spotify-song-evening"]').hide();
        break;
    }

    $("section[category='evening']").css("height", getContentHeight($("section[category='evening']")) + "px");
  });
}

function init_raplaSection() { 
  $("#btn-rapla-key").click(function (e) {
    e.preventDefault();
    var requestData = {
      "key": $('#input-rapla-key').val()
    };
    $("#btn-rapla-key").attr("disabled", true);
    $.ajax({
      type: "POST",
      url: "",
      data: JSON.stringify(requestData),
      contentType: 'application/json; charset=utf-8',
      success: function (response) {
        $('#btn-rapla-key').removeAttr("disabled");
      },
      error: function (error) {
        console.log(error);
        $('#btn-rapla-key').removeAttr("disabled");
      }
    });
  });
}

function init_testSection() { 
  $("#btn-test").click(function (e) {
    e.preventDefault();
    var requestData = {
      "text": $('#input-test').val()
    };
    var elapsedMS = 0;
    var testInterval = setInterval(function () {
      elapsedMS += 10;
      $('#test-result').html(elapsedMS + " ms | -");
    }, 10);
    $("#btn-test").attr("disabled", true);
    $.ajax({
      type: "POST",
      url: "/api/speechText",
      data: JSON.stringify(requestData),
      contentType: 'application/json; charset=utf-8',
      success: function (response) {
        clearInterval(testInterval);
        $('#test-result').html(elapsedMS + " ms | " + response);
        console.log(response);
        $('#btn-test').removeAttr("disabled");
      },
      error: function (error) {
        clearInterval(testInterval);
        $('#test-result').html(elapsedMS + " ms | Error: " + error.statusText);
        console.log(error);
        $('#btn-test').removeAttr("disabled");
      }
    });
  });
}

function adjustToContentHeight(element) {
  element.css("height", getContentHeight(element) + "px");
}

function getContentHeight(element) { 
  var height = 0;
  for(var i = 0; i < element.children().length; i++){
    var child = $(element.children()[i]);
    if(child.is(":visible"))
      height += child.outerHeight(true);
  }
  return height;
}