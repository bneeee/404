--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md543225d7c19e7b7bd74e416fe9566a7f9';






--
-- Databases
--

--
-- Database "template1" dump
--

\connect template1

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.0 (Debian 12.0-2.pgdg100+1)
-- Dumped by pg_dump version 12.0 (Debian 12.0-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- Database "digital_assistent" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.0 (Debian 12.0-2.pgdg100+1)
-- Dumped by pg_dump version 12.0 (Debian 12.0-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: digital_assistent; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE digital_assistent WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE digital_assistent OWNER TO postgres;

\connect digital_assistent

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: calendar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.calendar (
    id integer NOT NULL,
    title text,
    description text[],
    begintime timestamp without time zone,
    endtime timestamp without time zone,
    place text,
    type text,
    user_id integer
);


ALTER TABLE public.calendar OWNER TO postgres;

--
-- Name: calendar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.calendar_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calendar_id_seq OWNER TO postgres;

--
-- Name: calendar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.calendar_id_seq OWNED BY public.calendar.id;


--
-- Name: dualis_grades; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dualis_grades (
    unitname character varying(1023) NOT NULL,
    examname character varying(1023) NOT NULL,
    examdescription character varying(1023) NOT NULL,
    grade character varying(1023),
    attempt integer NOT NULL,
    date character varying(1023) NOT NULL,
    username character varying(1023) NOT NULL
);


ALTER TABLE public.dualis_grades OWNER TO postgres;

--
-- Name: dualis_grades_incoming; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dualis_grades_incoming (
    unitname character varying(1023) NOT NULL,
    examname character varying(1023) NOT NULL,
    examdescription character varying(1023) NOT NULL,
    grade character varying(1023),
    attempt integer NOT NULL,
    date character varying(1023) NOT NULL,
    username character varying(1023) NOT NULL
);


ALTER TABLE public.dualis_grades_incoming OWNER TO postgres;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notifications (
    text character varying(4095) NOT NULL,
    priority integer DEFAULT 2,
    date timestamp without time zone DEFAULT now(),
    played boolean DEFAULT false
);


ALTER TABLE public.notifications OWNER TO postgres;

--
-- Name: user_preferences; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_preferences (
    id integer NOT NULL,
    key text,
    value text,
    user_id integer
);


ALTER TABLE public.user_preferences OWNER TO postgres;

--
-- Name: user_preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_preferences_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_preferences_id_seq OWNER TO postgres;

--
-- Name: user_preferences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_preferences_id_seq OWNED BY public.user_preferences.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username text,
    password text,
    name text
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: calendar id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calendar ALTER COLUMN id SET DEFAULT nextval('public.calendar_id_seq'::regclass);


--
-- Name: user_preferences id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_preferences ALTER COLUMN id SET DEFAULT nextval('public.user_preferences_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: calendar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.calendar (id, title, description, begintime, endtime, place, type, user_id) FROM stdin;
9	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
10	Consulting und technischer Vertrieb	{"STG-TINF17B,RB41-4.18","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
11	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
12	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
13	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
14	Recht	{"STG-TINF17B,RB41-4.18","Klink, Eva,"}	2019-11-20 13:30:00	2019-11-20 16:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
15	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
16	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
17	Consulting und technischer Vertrieb	{"STG-TINF17B,RB41-4.18","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
18	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
19	Architekturen von Businesssystemen	{"STG-TINF17B,RB41-4.18","Seitter, Hartmut,,Richter, Wolfram,,Reichmann, Felix,"}	2019-11-19 11:00:00	2019-11-19 13:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
20	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
21	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
22	Data Warehouse	{"RB41-4.18,STG-TINF17B","Buckenhofer, Andreas,"}	2019-11-21 13:30:00	2019-11-21 16:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
23	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
24	Studientag	{STG-TINF17B}	2019-11-20 08:00:00	2019-11-20 18:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
25	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
26	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
27	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
28	Aufbau Studientag	{STG-TINF17B}	2019-11-19 12:00:00	2019-11-19 18:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
29	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
30	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
31	Consulting und technischer Vertrieb	{"STG-TINF17B,RB41-4.18","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
32	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
33	Architekturen von Businesssystemen	{"STG-TINF17B,RB41-4.18","Seitter, Hartmut,,Richter, Wolfram,,Reichmann, Felix,"}	2019-11-19 11:00:00	2019-11-19 13:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
34	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
35	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
36	Recht	{"STG-TINF17B,RB41-4.18","Späth-Weinreich, Annette,"}	2019-11-20 13:30:00	2019-11-20 16:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
37	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
38	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
39	Consulting und technischer Vertrieb	{"STG-TINF17B,RB41-4.18","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
40	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
41	Architekturen von Businesssystemen	{"STG-TINF17B,RB41-4.18","Seitter, Hartmut,,Richter, Wolfram,,Reichmann, Felix,"}	2019-11-19 11:00:00	2019-11-19 13:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
42	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
43	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
44	Recht	{"STG-TINF17B,RB41-4.18","Späth-Weinreich, Annette,"}	2019-11-20 13:30:00	2019-11-20 16:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
45	Data Warehouse	{"RB41-4.18,STG-TINF17B","Buckenhofer, Andreas,"}	2019-11-21 14:15:00	2019-11-21 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
46	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
47	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
48	Consulting und technischer Vertrieb	{"RB41-4.18,STG-TINF17B","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
49	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
50	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
51	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
52	Recht	{"STG-TINF17B,RB41-4.18","Späth-Weinreich, Annette,"}	2019-11-20 13:30:00	2019-11-20 16:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
53	Data Warehouse	{"RB41-4.18,STG-TINF17B","Buckenhofer, Andreas,"}	2019-11-21 14:15:00	2019-11-21 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
54	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
55	Klausuren	{STG-TINF17B}	2019-11-18 08:00:00	2019-11-18 18:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
56	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
57	Consulting und technischer Vertrieb	{"STG-TINF17B,RB41-4.18","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
58	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
59	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
60	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
61	Recht	{"STG-TINF17B,RB41-4.18","Klink, Eva,"}	2019-11-20 13:30:00	2019-11-20 16:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
62	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
63	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
64	Consulting und technischer Vertrieb	{"STG-TINF17B,RB41-4.18","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
65	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
66	Architekturen von Businesssystemen	{"STG-TINF17B,RB41-4.18","Seitter, Hartmut,,Richter, Wolfram,,Reichmann, Felix,"}	2019-11-19 11:00:00	2019-11-19 13:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
67	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
68	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
69	Data Warehouse	{"RB41-4.18,STG-TINF17B","Buckenhofer, Andreas,"}	2019-11-21 13:30:00	2019-11-21 16:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
70	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
71	Studientag	{STG-TINF17B}	2019-11-20 08:00:00	2019-11-20 18:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
72	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
73	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
74	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
75	Aufbau Studientag	{STG-TINF17B}	2019-11-19 12:00:00	2019-11-19 18:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
76	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
77	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
78	Consulting und technischer Vertrieb	{"STG-TINF17B,RB41-4.18","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
79	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
80	Architekturen von Businesssystemen	{"STG-TINF17B,RB41-4.18","Seitter, Hartmut,,Richter, Wolfram,,Reichmann, Felix,"}	2019-11-19 11:00:00	2019-11-19 13:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
81	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
82	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
83	Recht	{"STG-TINF17B,RB41-4.18","Späth-Weinreich, Annette,"}	2019-11-20 13:30:00	2019-11-20 16:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
84	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
85	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
86	Consulting und technischer Vertrieb	{"STG-TINF17B,RB41-4.18","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
87	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
88	Architekturen von Businesssystemen	{"STG-TINF17B,RB41-4.18","Seitter, Hartmut,,Richter, Wolfram,,Reichmann, Felix,"}	2019-11-19 11:00:00	2019-11-19 13:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
89	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
90	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
91	Recht	{"STG-TINF17B,RB41-4.18","Späth-Weinreich, Annette,"}	2019-11-20 13:30:00	2019-11-20 16:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
92	Data Warehouse	{"RB41-4.18,STG-TINF17B","Buckenhofer, Andreas,"}	2019-11-21 14:15:00	2019-11-21 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
93	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
94	Advanced Software Engineering	{"RB41-4.18,STG-TINF17B","Rößler, Horst,"}	2019-11-21 08:15:00	2019-11-21 11:45:00	Rotebühlplatz 41B, Stuttgart	lecture	1
95	Consulting und technischer Vertrieb	{"RB41-4.18,STG-TINF17B","Stark, Wolfgang,"}	2019-11-20 09:00:00	2019-11-20 12:15:00	Rotebühlplatz 41B, Stuttgart	lecture	1
96	IT-Sicherheit	{"RB41-4.18,STG-TINF17B","Jost, Werner, Dr."}	2019-11-18 09:15:00	2019-11-18 12:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
97	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-22 11:30:00	2019-11-22 14:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
98	Wahlfächer/Zusatzfächer	{STG-TINF17B}	2019-11-18 13:15:00	2019-11-18 18:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
99	Recht	{"STG-TINF17B,RB41-4.18","Späth-Weinreich, Annette,"}	2019-11-20 13:30:00	2019-11-20 16:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
100	Data Warehouse	{"RB41-4.18,STG-TINF17B","Buckenhofer, Andreas,"}	2019-11-21 14:15:00	2019-11-21 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
101	Architekturen von Rechnersystemen	{"RB41-4.18,STG-TINF17B","Bachmaier, Martin,,Fütterling, Stefan, Dr."}	2019-11-19 15:00:00	2019-11-19 17:30:00	Rotebühlplatz 41B, Stuttgart	lecture	1
102	Klausuren	{STG-TINF17B}	2019-11-18 08:00:00	2019-11-18 18:00:00	Rotebühlplatz 41B, Stuttgart	lecture	1
\.


--
-- Data for Name: dualis_grades; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dualis_grades (unitname, examname, examdescription, grade, attempt, date, username) FROM stdin;
T3_3000  Praxisprojekt III (SoSe 2020)	Hausarbeit (1%)	T3_3000.1 Projektarbeit III (STG-TINF17B)	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3_3000  Praxisprojekt III (SoSe 2020)	Ablauf- und Reflexionsbericht (1%)	T3_3000.2 Wissenschaftliches Arbeiten III (STG-TINF17B)	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3_3101  Studienarbeit (SoSe 2020)	Studienarbeit (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3INF4305  SoftwarequalitÃ¤t und Verteilte Systeme (SoSe 2020)	Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3INF4323  KÃ¼nstliche Intelligenz und interaktive Systeme (SoSe 2020)	Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3INF3001  Software Engineering II (WiSe 2019/20)	Programmentwurf (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF3002  IT-Sicherheit (WiSe 2019/20)	Klausur (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF4304  Datenbanken II (WiSe 2019/20)	Klausur (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF4322  Architekturen (WiSe 2019/20)	Klausur (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF4324  Consulting, technischer Vetrieb und Recht (WiSe 2019/20)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF4902  Wahlmodul Informatik II (WiSe 2019/20)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3_2000  Praxisprojekt II (SoSe 2019)	Projektarbeit (50%)	T3_2000.1 Projektarbeit II (STG-TINF17B	noch nicht gesetzt	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3_2000  Praxisprojekt II (SoSe 2019)	MÃ¼ndliche PrÃ¼fung (50%)	T3_2000.2 MÃ¼ndliche PrÃ¼fung (STG-TINF17B	1,5	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3_2000  Praxisprojekt II (SoSe 2019)	Ablauf- und Reflexionsbericht (0%)	T3_2000.3 Wissenschaftliches Arbeiten II (STG-TINF17B	noch nicht gesetzt	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2001  Mathematik II (WiSe 2018/19)	Klausur (50%)	T3INF2001.1 Angewandte Mathematik (STG-TINF17B)	1,0	1	WiSe 2018/19	inf17157@lehre.dhbw-stuttgart.de
T3INF2001  Mathematik II (WiSe 2018/19)	Klausur (50%)	T3INF2001.2 Statistik (STG-TINF17B)	1,2	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2002  Theoretische Informatik III (SoSe 2019)	Klausur (100%)	Modulabschlussleistungen	1,4	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2003  Software Engineering I (SoSe 2019)	Programmentwurf (100%)	Modulabschlussleistungen	1,6	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2004  Datenbanken (SoSe 2019)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	1,5	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2006  Kommunikations- und Netztechnik (STG-TINF17B) (SoSe 2019)	Klausur (100%)	Modulabschlussleistungen	1,8	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF4212  Web-Engineering II (SoSe 2019)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	1,7	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF4901  Wahlmodul Informatik (STG Jahr 2) (SoSe 2019)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	1,1	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF4901  Wahlmodul Informatik (STG Jahr 2) (SoSe 2019)	Wahlunit (50%)	SoSe 2019	100,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF4901  Wahlmodul Informatik (STG Jahr 2) (SoSe 2019)	Netztechniken der Zugangs- und Weitverkehrsnetzte (50%)		96,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF2005  Technische Informatik II (STG-TINF17B) (WiSe 2018/19)	Klausur (100%)	Modulabschlussleistungen	1,9	1	WiSe 2018/19	inf17157@lehre.dhbw-stuttgart.de
T3INF2005  Technische Informatik II (STG-TINF17B) (WiSe 2018/19)	Betriebssysteme (40%)	WiSe 2018/19	100,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF2005  Technische Informatik II (STG-TINF17B) (WiSe 2018/19)	Rechnerarchitekturen (40%)		80,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF2005  Technische Informatik II (STG-TINF17B) (WiSe 2018/19)	Systemnahe Programmierung (20%)		66,0	1		inf17157@lehre.dhbw-stuttgart.de
T3_1000  Praxisprojekt I (SoSe 2018)	Projektarbeit (1%)	T3_1000.1 Projektarbeit I (STG-TINF17)	b	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3_1000  Praxisprojekt I (SoSe 2018)	Ablauf- und Reflexionsbericht (1%)	T3_1000.2 Wissenschaftliches Arbeiten I (STG-TINF17)	b	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1001  Mathematik I (WiSe 2017/18)	Klausurarbeit (100%)	T3INF1001.1 Lineare Algebra (STG-TINF17B)	2,0	1	WiSe 2017/18	inf17157@lehre.dhbw-stuttgart.de
T3INF1001  Mathematik I (WiSe 2017/18)	Klausurarbeit (100%)	T3INF1001.2 Analysis (STG-TINF17B)	2,7	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1003  Theoretische Informatik II (SoSe 2018)	Klausurarbeit (100%)	Modulabschlussleistungen	2,6	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1004  Programmieren (SoSe 2018)	Programmentwurf (100%)	Modulabschlussleistungen	1,5	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1004  Programmieren (SoSe 2018)	PrÃ¼fung 1. Semester (50%)	SoSe 2018	92,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF1004  Programmieren (SoSe 2018)	PrÃ¼fung 2. Semester (50%)		91,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF1005  SchlÃ¼sselqualifikationen (SoSe 2018)	Klausurarbeit (< 50 %) (100%)	Modulabschlussleistungen	1,7	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1005  SchlÃ¼sselqualifikationen (SoSe 2018)	BWL (50%)	SoSe 2018	90,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF1005  SchlÃ¼sselqualifikationen (SoSe 2018)	Marketing (50%)		86,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF4101  Web Engineering (SoSe 2018)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	1,9	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF4103  Anwendungsprojekt Informatik (SoSe 2018)	Klausurarbeit < 50 % (100%)	Modulabschlussleistungen	2,3	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF9011  SchlÃ¼sselqualifikationen II (SoSe 2018)	Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	2,0	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF9011  SchlÃ¼sselqualifikationen II (SoSe 2018)	Intercultural Communication (80%)	SoSe 2018	87,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF9011  SchlÃ¼sselqualifikationen II (SoSe 2018)	Forschungsmethoden (20%)		66,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF1002  Theoretische Informatik I (WiSe 2017/18)	Klausurarbeit (100%)	Modulabschlussleistungen	2,4	1	WiSe 2017/18	inf17157@lehre.dhbw-stuttgart.de
T3INF1006  Technische Informatik I (WiSe 2017/18)	Klausurarbeit (100%)	Modulabschlussleistungen	1,0	1	WiSe 2017/18	inf17157@lehre.dhbw-stuttgart.de
test2	test2	test2	nicht gesetetzt	1	Sose 2019	inf17157@lehre.dhbw-stuttgart.de
test	test	test	1.0	1	Sose 2019	inf17157@lehre.dhbw-stuttgart.de
\.


--
-- Data for Name: dualis_grades_incoming; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dualis_grades_incoming (unitname, examname, examdescription, grade, attempt, date, username) FROM stdin;
T3_3000  Praxisprojekt III (SoSe 2020)	Hausarbeit (1%)	T3_3000.1 Projektarbeit III (STG-TINF17B)	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3_3000  Praxisprojekt III (SoSe 2020)	Ablauf- und Reflexionsbericht (1%)	T3_3000.2 Wissenschaftliches Arbeiten III (STG-TINF17B)	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3_3101  Studienarbeit (SoSe 2020)	Studienarbeit (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3INF4305  SoftwarequalitÃ¤t und Verteilte Systeme (SoSe 2020)	Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3INF4323  KÃ¼nstliche Intelligenz und interaktive Systeme (SoSe 2020)	Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	SoSe 2020	inf17157@lehre.dhbw-stuttgart.de
T3INF3001  Software Engineering II (WiSe 2019/20)	Programmentwurf (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF3002  IT-Sicherheit (WiSe 2019/20)	Klausur (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF4304  Datenbanken II (WiSe 2019/20)	Klausur (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF4322  Architekturen (WiSe 2019/20)	Klausur (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF4324  Consulting, technischer Vetrieb und Recht (WiSe 2019/20)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3INF4902  Wahlmodul Informatik II (WiSe 2019/20)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	noch nicht gesetzt	1	WiSe 2019/20	inf17157@lehre.dhbw-stuttgart.de
T3_2000  Praxisprojekt II (SoSe 2019)	Projektarbeit (50%)	T3_2000.1 Projektarbeit II (STG-TINF17B	noch nicht gesetzt	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3_2000  Praxisprojekt II (SoSe 2019)	MÃ¼ndliche PrÃ¼fung (50%)	T3_2000.2 MÃ¼ndliche PrÃ¼fung (STG-TINF17B	1,5	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3_2000  Praxisprojekt II (SoSe 2019)	Ablauf- und Reflexionsbericht (0%)	T3_2000.3 Wissenschaftliches Arbeiten II (STG-TINF17B	noch nicht gesetzt	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2001  Mathematik II (WiSe 2018/19)	Klausur (50%)	T3INF2001.1 Angewandte Mathematik (STG-TINF17B)	1,0	1	WiSe 2018/19	inf17157@lehre.dhbw-stuttgart.de
T3INF2001  Mathematik II (WiSe 2018/19)	Klausur (50%)	T3INF2001.2 Statistik (STG-TINF17B)	1,2	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2002  Theoretische Informatik III (SoSe 2019)	Klausur (100%)	Modulabschlussleistungen	1,4	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2003  Software Engineering I (SoSe 2019)	Programmentwurf (100%)	Modulabschlussleistungen	1,6	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2004  Datenbanken (SoSe 2019)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	1,5	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF2006  Kommunikations- und Netztechnik (STG-TINF17B) (SoSe 2019)	Klausur (100%)	Modulabschlussleistungen	1,8	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF4212  Web-Engineering II (SoSe 2019)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	1,7	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF4901  Wahlmodul Informatik (STG Jahr 2) (SoSe 2019)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	1,1	1	SoSe 2019	inf17157@lehre.dhbw-stuttgart.de
T3INF4901  Wahlmodul Informatik (STG Jahr 2) (SoSe 2019)	Wahlunit (50%)	SoSe 2019	100,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF4901  Wahlmodul Informatik (STG Jahr 2) (SoSe 2019)	Netztechniken der Zugangs- und Weitverkehrsnetzte (50%)		96,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF2005  Technische Informatik II (STG-TINF17B) (WiSe 2018/19)	Klausur (100%)	Modulabschlussleistungen	1,9	1	WiSe 2018/19	inf17157@lehre.dhbw-stuttgart.de
T3INF2005  Technische Informatik II (STG-TINF17B) (WiSe 2018/19)	Betriebssysteme (40%)	WiSe 2018/19	100,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF2005  Technische Informatik II (STG-TINF17B) (WiSe 2018/19)	Rechnerarchitekturen (40%)		80,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF2005  Technische Informatik II (STG-TINF17B) (WiSe 2018/19)	Systemnahe Programmierung (20%)		66,0	1		inf17157@lehre.dhbw-stuttgart.de
T3_1000  Praxisprojekt I (SoSe 2018)	Projektarbeit (1%)	T3_1000.1 Projektarbeit I (STG-TINF17)	b	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3_1000  Praxisprojekt I (SoSe 2018)	Ablauf- und Reflexionsbericht (1%)	T3_1000.2 Wissenschaftliches Arbeiten I (STG-TINF17)	b	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1001  Mathematik I (WiSe 2017/18)	Klausurarbeit (100%)	T3INF1001.1 Lineare Algebra (STG-TINF17B)	2,0	1	WiSe 2017/18	inf17157@lehre.dhbw-stuttgart.de
T3INF1001  Mathematik I (WiSe 2017/18)	Klausurarbeit (100%)	T3INF1001.2 Analysis (STG-TINF17B)	2,7	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1003  Theoretische Informatik II (SoSe 2018)	Klausurarbeit (100%)	Modulabschlussleistungen	2,6	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1004  Programmieren (SoSe 2018)	Programmentwurf (100%)	Modulabschlussleistungen	1,5	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1004  Programmieren (SoSe 2018)	PrÃ¼fung 1. Semester (50%)	SoSe 2018	92,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF1004  Programmieren (SoSe 2018)	PrÃ¼fung 2. Semester (50%)		91,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF1005  SchlÃ¼sselqualifikationen (SoSe 2018)	Klausurarbeit (< 50 %) (100%)	Modulabschlussleistungen	1,7	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF1005  SchlÃ¼sselqualifikationen (SoSe 2018)	BWL (50%)	SoSe 2018	90,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF1005  SchlÃ¼sselqualifikationen (SoSe 2018)	Marketing (50%)		86,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF4101  Web Engineering (SoSe 2018)	Klausurarbeit oder Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	1,9	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF4103  Anwendungsprojekt Informatik (SoSe 2018)	Klausurarbeit < 50 % (100%)	Modulabschlussleistungen	2,3	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF9011  SchlÃ¼sselqualifikationen II (SoSe 2018)	Kombinierte PrÃ¼fung (100%)	Modulabschlussleistungen	2,0	1	SoSe 2018	inf17157@lehre.dhbw-stuttgart.de
T3INF9011  SchlÃ¼sselqualifikationen II (SoSe 2018)	Intercultural Communication (80%)	SoSe 2018	87,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF9011  SchlÃ¼sselqualifikationen II (SoSe 2018)	Forschungsmethoden (20%)		66,0	1		inf17157@lehre.dhbw-stuttgart.de
T3INF1002  Theoretische Informatik I (WiSe 2017/18)	Klausurarbeit (100%)	Modulabschlussleistungen	2,4	1	WiSe 2017/18	inf17157@lehre.dhbw-stuttgart.de
T3INF1006  Technische Informatik I (WiSe 2017/18)	Klausurarbeit (100%)	Modulabschlussleistungen	1,0	1	WiSe 2017/18	inf17157@lehre.dhbw-stuttgart.de
test	test	test	1.0	1	Sose 2019	inf17157@lehre.dhbw-stuttgart.de
test2	test2	test2	nicht gesetetzt	1	Sose 2019	inf17157@lehre.dhbw-stuttgart.de
\.


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notifications (text, priority, date, played) FROM stdin;
Es wurde eine Note für das Modul test veröffentlicht. Die Bewertung lautet: 1.0	2	2019-10-21 16:57:19.65944	f
\.


--
-- Data for Name: user_preferences; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_preferences (id, key, value, user_id) FROM stdin;
1	spotifyToken	lschwwuuullzub	1
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, username, password, name) FROM stdin;
1	Test	jkghedckuzebgdczujfzjue	Test
\.


--
-- Name: calendar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.calendar_id_seq', 102, true);


--
-- Name: user_preferences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_preferences_id_seq', 3, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: calendar calendar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calendar
    ADD CONSTRAINT calendar_pkey PRIMARY KEY (id);


--
-- Name: dualis_grades_incoming dualis_grades_incoming_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dualis_grades_incoming
    ADD CONSTRAINT dualis_grades_incoming_pkey PRIMARY KEY (unitname, examname, date, examdescription, attempt, username);


--
-- Name: dualis_grades dualis_grades_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dualis_grades
    ADD CONSTRAINT dualis_grades_pkey PRIMARY KEY (unitname, examname, date, examdescription, attempt, username);


--
-- Name: user_preferences user_preferences_key_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_preferences
    ADD CONSTRAINT user_preferences_key_user_id_key UNIQUE (key, user_id);


--
-- Name: user_preferences user_preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_preferences
    ADD CONSTRAINT user_preferences_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: calendar calendar_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calendar
    ADD CONSTRAINT calendar_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: user_preferences user_preferences_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_preferences
    ADD CONSTRAINT user_preferences_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

\connect postgres

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.0 (Debian 12.0-2.pgdg100+1)
-- Dumped by pg_dump version 12.0 (Debian 12.0-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

