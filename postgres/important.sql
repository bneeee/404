SELECT * FROM dualis_grades_incoming
EXCEPT
SELECT * FROM dualis_grades;

INSERT INTO 

ON CONFLICT 

unitname examname examdescription grade attempt date username  

[1-5],[0-9]|6.0|[1-5].[0-9]|6,0|100|[0-9]?[0-9]|bestanden

BEGIN;
WITH X AS (
    select * from dualis_grades_incoming 
    except
    select * from dualis_grades
),
Y AS (
    INSERT INTO dualis_grades
    select * from X
    ON Conflict (unitname,examname,examdescription,attempt,date,username)
    DO UPDATE
    SET grade = EXCLUDED.grade
    returning *
)
INSERT INTO notifications (text,priority)
SELECT 'Es wurde eine Note für das Modul ' || Y.unitname || ' veröffentlicht. Die Bewertung lautet: ' || Y.grade AS text, 2 AS priority 
FROM Y
WHERE Y.grade similar to '[1-5],[0-9]|6.0|[1-5].[0-9]|6,0|100|[0-9]?[0-9]|bestanden';
--(╯°□°）╯︵ ┻━┻ - boom mathafucka
END;


BEGIN;
WITH X AS (
    select * from dualis_grades_incoming 
    except
    select * from dualis_grades
),
Y AS (
    INSERT INTO dualis_grades
    select * from X
    ON Conflict (unitname,examname,examdescription,attempt,date,username)
    DO UPDATE
    SET grade = EXCLUDED.grade
    returning *
)
SELECT 'Es wurde eine Note für das Modul ' || Y.unitname || ' veröffentlicht. Die Bewertung lautet: ' || Y.grade AS text, 2 AS priority
FROM Y
WHERE Y.grade similar to '[1-5],[0-9]|6.0|[1-5].[0-9]|6,0|100|[0-9]?[0-9]|bestanden';
END;
